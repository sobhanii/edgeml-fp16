import tensorflow as tf
from tensorflow.keras.datasets import mnist
import numpy as np

# Load the MNIST dataset for testing
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = tf.keras.models.load_model('tiny_cnn_model_fp16.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)

# Optionally, you can evaluate the model's accuracy on the test data
y_test_categorical = tf.keras.utils.to_categorical(y_test, num_classes=10)
loss, accuracy = loaded_model.evaluate(x_test, y_test_categorical, verbose=0)
print(f'Loss: {loss:.4f}, Accuracy: {accuracy * 100:.2f}%')
