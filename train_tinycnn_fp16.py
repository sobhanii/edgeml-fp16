import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0  # Normalize pixel values to [0, 1]
y_train, y_test = to_categorical(y_train), to_categorical(y_test)

# Reshape the data to add a channel dimension for Conv2D
x_train = x_train.reshape(x_train.shape[0], 28, 28, 1).astype('float32')
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1).astype('float32')

# Convert the data to mixed precision
x_train = tf.convert_to_tensor(x_train)
x_test = tf.convert_to_tensor(x_test)
y_train = tf.convert_to_tensor(y_train)
y_test = tf.convert_to_tensor(y_test)

# Build a Tiny CNN model
model = Sequential([
    Conv2D(32, (3, 3), activation='relu', input_shape=(28, 28, 1)),
    MaxPooling2D((2, 2)),
    Flatten(),
    Dense(128, activation='relu'),
    Dropout(0.5),
    Dense(10, activation='softmax')
])

# Define the optimizer with mixed precision
optimizer = tf.keras.optimizers.Adam()

# Use mixed precision policy
policy = tf.keras.mixed_precision.Policy('mixed_float16')
tf.keras.mixed_precision.set_global_policy(policy)

# Compile the model with the mixed precision optimizer
model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(x_train, y_train, epochs=5, batch_size=64, validation_split=0.2)

# Save the trained model
model.save('tiny_cnn_model_mixed_precision.h5')

# Load the saved model
loaded_model = tf.keras.models.load_model('tiny_cnn_model_mixed_precision.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)

# Optionally, you can evaluate the model's accuracy on the test data
loss, accuracy = loaded_model.evaluate(x_test, y_test, verbose=0)
print(f'Loss: {loss:.4f}, Accuracy: {accuracy * 100:.2f}%')
