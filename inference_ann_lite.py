import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.datasets import mnist
import numpy as np

# Load the MNIST dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(x_test.shape[0], -1)

# Load the saved model
loaded_model = load_model('neural_network_model_float16.h5')

# Convert the loaded model to a TensorFlow Lite quantized model with FP16
converter = tf.lite.TFLiteConverter.from_keras_model(loaded_model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.target_spec.supported_types = [tf.float16]
quantized_tflite_model = converter.convert()

# Save the quantized model to a file
with open('quantized_neural_network_model_fp16.tflite', 'wb') as f:
    f.write(quantized_tflite_model)

# Load the quantized model
interpreter = tf.lite.Interpreter(model_content=quantized_tflite_model)
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Run inference on the test data using the quantized model
correct_predictions = 0

for i in range(len(x_test)):
    input_shape = input_details[0]['shape']
    input_data = x_test[i].reshape(input_shape).astype(np.float32)
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    predictions = interpreter.get_tensor(output_details[0]['index'])
    predicted_label = np.argmax(predictions)
    if predicted_label == y_test[i]:
        correct_predictions += 1

accuracy = correct_predictions / len(x_test)
print(f'Accuracy (Quantized Model with FP16): {accuracy * 100:.2f}%')
