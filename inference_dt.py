import joblib
from sklearn.metrics import accuracy_score
from tensorflow.keras.datasets import mnist
import numpy as np

# Load the MNIST dataset
_, (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(x_test.shape[0], -1)

# Quantize the data to 16-bit precision
x_test_quantized = np.float16(x_test)

# Load the saved Decision Tree model
loaded_model = joblib.load('decision_tree_model_quantized.joblib')

# Make predictions on the quantized test data
y_pred = loaded_model.predict(x_test_quantized)

# Calculate the accuracy of the loaded Decision Tree model
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy (Loaded Model): {accuracy * 100:.2f}%')
