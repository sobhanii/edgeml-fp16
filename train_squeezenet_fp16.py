import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Dropout, GlobalAveragePooling2D
from tensorflow.keras.layers import Activation, Concatenate, Input
from tensorflow.keras.utils import to_categorical  # Import to_categorical
from tensorflow.keras import Model

# Load and preprocess the MNIST dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0  # Normalize pixel values to [0, 1]

# Reshape the data to add a channel dimension
x_train = x_train.reshape(x_train.shape[0], 28, 28, 1)
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# One-hot encode the target labels
y_train = to_categorical(y_train, num_classes=10)  # 10 is the number of classes in MNIST
y_test = to_categorical(y_test, num_classes=10)

# Define the fire_module function
def fire_module(x, squeeze_filters, expand_filters):
    squeeze = Conv2D(squeeze_filters, (1, 1), activation='relu')(x)
    expand_1x1 = Conv2D(expand_filters, (1, 1), activation='relu')(squeeze)
    expand_3x3 = Conv2D(expand_filters, (3, 3), activation='relu', padding='same')(squeeze)
    output = Concatenate()([expand_1x1, expand_3x3])
    return output

# Build SqueezeNet model
input_image = Input(shape=(28, 28, 1))
x = Conv2D(64, (3, 3), strides=(2, 2), activation='relu')(input_image)
x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

x = fire_module(x, 16, 64)
x = fire_module(x, 16, 64)
x = fire_module(x, 32, 128)
x = MaxPooling2D(pool_size=(2, 2))(x)

x = fire_module(x, 32, 128)
x = fire_module(x, 48, 192)
x = fire_module(x, 48, 192)
x = fire_module(x, 64, 256)
x = MaxPooling2D(pool_size=(2, 2))(x)

x = fire_module(x, 64, 256)
x = Conv2D(10, (1, 1))(x)  # Change the number of units to 10
x = GlobalAveragePooling2D()(x)
output = Activation('softmax')(x)

model = Model(inputs=input_image, outputs=output)

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(x_train, y_train, epochs=5, batch_size=64, validation_split=0.2)

model.save('squeezenet_model.h5')

# Convert the model to a TensorFlow Lite model (FP16 quantization)
converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.target_spec.supported_types = [tf.float16]
tflite_model = converter.convert()

# Save the TensorFlow Lite model to a file
with open('squeezenet_model_fp16.tflite', 'wb') as f:
    f.write(tflite_model)
