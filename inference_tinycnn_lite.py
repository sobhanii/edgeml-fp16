import tensorflow as tf
import numpy as np
from tensorflow.keras.datasets import mnist

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float16)  # Convert to FP16
y_test = tf.keras.utils.to_categorical(y_test).astype(np.float16)  # Convert to FP16

# Load the TensorFlow Lite FP16 model
interpreter = tf.lite.Interpreter(model_path='tiny_cnn_model_fp16.tflite')
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Initialize accuracy counter
tflite_accuracy = 0.0

# Perform inference using the TensorFlow Lite FP16 model
for i in range(len(x_test)):
    interpreter.set_tensor(input_details[0]['index'], x_test[i:i+1])
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    tflite_accuracy += np.argmax(tflite_predictions) == np.argmax(y_test[i:i+1])

tflite_accuracy /= len(x_test)
print(f'TFLite FP16 Model Accuracy: {tflite_accuracy * 100:.2f}%')
