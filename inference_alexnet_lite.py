import numpy as np
import tensorflow as tf
from tensorflow.keras.datasets import mnist

# Load the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float32)  # Reshape and cast to float32
y_test = tf.keras.utils.to_categorical(y_test, num_classes=10)  # One-hot encode labels

# Load the TensorFlow Lite model
interpreter = tf.lite.Interpreter(model_path='alexnet_model_fp16.tflite')
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Initialize accuracy counter
tflite_accuracy = 0.0

# Perform inference on the test data
for i in range(len(x_test)):
    interpreter.set_tensor(input_details[0]['index'], x_test[i:i+1])
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    tflite_accuracy += np.argmax(y_test[i:i+1], axis=1) == np.argmax(tflite_predictions, axis=1)

tflite_accuracy /= len(x_test)
tflite_accuracy = float(tflite_accuracy)  # Convert to float
print(f'TFLite Model Accuracy: {tflite_accuracy * 100:.2f}%')
