import numpy as np
import tensorflow as tf
from tensorflow.keras.datasets import mnist

# Load the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(-1, 28, 28, 1).astype(np.float32)  # Reshape and cast to float32
y_test = y_test.astype(np.float32)  # Cast labels to float32

# Load the TensorFlow Lite model
interpreter = tf.lite.Interpreter(model_path='r_cnn_model_fp16.tflite')
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Initialize variables for accuracy calculation
correct_predictions = 0
total_samples = len(x_test)

# Perform inference on the test data
for i in range(len(x_test)):
    interpreter.set_tensor(input_details[0]['index'], x_test[i:i+1])
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    predicted_label = np.argmax(tflite_predictions)
    true_label = y_test[i]
    
    # Check if the prediction matches the true label
    if predicted_label == true_label:
        correct_predictions += 1

# Calculate accuracy
accuracy = correct_predictions / total_samples * 100
print(f'TFLite Model Accuracy: {accuracy:.2f}%')
